## WPezPlugins: WooCommerce Templates

__A much improved version of the template-centric stuff from the WooCommerce's Theme Customizations plugin__ 


> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --



### _IMPORTANT_

_This is boilerplate code, a developer's tool. You'll be adding code (templates to this)._

_If you're not a developer familiar with WordPress themes and such then this tool probably isn't for you._




### OVERVIEW

The key improvements over the WooCommerce plugin / version is:

1. This is strictly for templates. Your scripts and styles are decoupled (so to speak) and can be maintained elsewhere.

2. The folder structure is far more intuitive. The /theme folder is for theme templates. The /woocommerce/templates folder is for altering that plugin's folder.


### FAQ

__1 - Why?__

If you're using a WordPress WooCommerce child theme (e.g., Bistro) and want to make template changes you need to do that outside Bistro. Making the changes to Bistro itself means you'd lose your work if Bistro were updated.

 __2 - I'm not a developer, can we hire you?__
 
 Yes, that's always a possibility.


### HELPFUL LINKS

- https://github.com/woocommerce/theme-customisations

- 
 
 
### TODO

- 

### CHANGE LOG

__-- 0.0.1__

- INIT - hey! ho!! let's go!!!