<?php

namespace WPezWooCommerceTemplates;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

if ( ! class_exists('ClassFilters')){
    class ClassFilters{

        protected $_str_plugin_dir_path;
        protected $_str_tlt;
        protected $_str_wcto;


        public function __construct( $str = '', $str_tlt = 'theme', $str_wcto = 'woocommerce/templates' ) {

            $this->_str_plugin_dir_path = $str;
            $this->_str_tlt = $str_tlt;
            $this->_str_wcto = $str_wcto;

        }

        /**
         * Look in this plugin for template files first.
         * This works for the top level templates (IE single.php, page.php etc). However, it doesn't work for
         * template parts yet (content.php, header.php etc).
         *
         * Relevant trac ticket; https://core.trac.wordpress.org/ticket/13239
         *
         * @param  string $template template string.
         * @return string $template new template string.
         */
        public function themeCustomisationsTemplate( $template ) {


            // is the template passed to the filter from the theme or from the wc plugin?
            $length = strlen( dirname($this->_str_plugin_dir_path) );
            $str_slug = $this->_str_tlt;
            if ( (substr($template, 0, $length) == dirname($this->_str_plugin_dir_path) ) ){
               $str_slug = $this->_str_wcto;
            }

            if ( file_exists( untrailingslashit( $this->_str_plugin_dir_path ) . '/' . $str_slug .  '/' . basename( $template ) ) ) {
                $template = untrailingslashit( $this->_str_plugin_dir_path ) . '/' . $str_slug . '/' . basename( $template );
            }
            return $template;
        }

        /**
         * Look in this plugin for WooCommerce template overrides.
         *
         * For example, if you want to override woocommerce/templates/cart/cart.php, you
         * can place the modified template in <plugindir>/custom/templates/woocommerce/cart/cart.php
         *
         * @param string $located is the currently located template, if any was found so far.
         * @param string $template_name is the name of the template (ex: cart/cart.php).
         * @return string $located is the newly located template if one was found, otherwise
         *                         it is the previously found template.
         */
        public function themeCustomisationsWCGetTemplate( $located, $template_name, $args, $template_path, $default_path ) {

            $plugin_template_path = untrailingslashit( $this->_str_plugin_dir_path ) . '/' . $this->_str_wcto . '/' . $template_name;
            if ( file_exists( $plugin_template_path ) ) {
                $located = $plugin_template_path;
            }

          //  var_dump( $plugin_template_path);
            return $located;
        }


    }



}