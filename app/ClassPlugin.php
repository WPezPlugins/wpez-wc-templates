<?php


namespace WPezWooCommerceTemplates;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

if ( ! class_exists( 'ClassPlugin' ) ) {
    class ClassPlugin {


        protected $_new_filters;

        public function __construct( $str ) {

            require_once 'filters/ClassFilters.php';

            $this->_new_filters = new ClassFilters( $str );

           //  $this->filters();
           add_action( 'init', [ $this, 'filters' ], -1 );

        }


        public function filters() {

            add_filter( 'template_include', [ $this->_new_filters, 'themeCustomisationsTemplate' ], 11 );
            add_filter( 'wc_get_template', [ $this->_new_filters, 'themeCustomisationsWCGetTemplate' ], 11, 5 );
        }


    }
}