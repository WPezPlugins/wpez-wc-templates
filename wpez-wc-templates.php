<?php
/*
Plugin Name: WPezPlugins: WooCommerce Templates
Plugin URI: https://gitlab.com/WPezPlugins/wpez-wc-templates
Description: A much improved version of the template-centric stuff from the WooCommerce's Theme Customizations plugin
Version: 0.0.5
Author: Mark "Chief Alchemist" Simchock for Alchemy United
Author URI: https://AlchemyUnited.com
License: GPLv2 or later
Text Domain: TODO
*/

namespace WPezWooCommerceTemplates;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

function plugin($bool = true){

    if ( $bool !== true ) {
        return;
    }

    include "app/ClassPlugin.php";

    $str_plugin_dir_path = untrailingslashit(plugin_dir_path(__FILE__));
    $new_plugin = new ClassPlugin( $str_plugin_dir_path );
}


/**
 * Initialise the plugin
 */
// add_action( 'plugins_loaded', 'WPezWooCommerceTemplates\plugin' );

plugin();
